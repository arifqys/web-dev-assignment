export const chartData = {
  type: 'bar',
  data: {
    labels: ['October', 'November', 'December', 'January', 'February', 'March'],
    datasets: [{
        label: 'Nett',
        data: [26000, 23000, 28000, 30000, 40000, 13000],
        backgroundColor: [
          '#37B04C',
          '#37B04C',
          '#37B04C',
          '#37B04C',
          '#37B04C',
          '#37B04C'
        ],
        borderColor: [
          '#289E45',
          '#289E45',
          '#289E45',
          '#289E45',
          '#289E45',
          '#289E45',
        ],
        borderWidth: 3
      },
    ]
  },
  options: {
    responsive: true,
    lineTension: 1,
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true,
          padding: 25,
        }
      }]
    }
  }
}

export default chartData