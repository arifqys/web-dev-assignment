export const products = {
  bestSelling: [
    {
      name: 'Mineral Water',
      price: 'Rp 5.000',
      sold_qty: 5030234,
      img_url: 'https://ecs7.tokopedia.net/img/cache/700/product-1/2019/6/11/1874896/1874896_754d1c3c-4c7a-4dd0-802c-6704abde7c94_554_554.jpg',
    },
    {
      name: 'Soap',
      price: 'Rp 23.000',
      sold_qty: 4305023,
      img_url: 'https://upload.wikimedia.org/wikipedia/commons/9/9b/Handmade_soap_cropped_and_simplified.jpg',
    },
    {
      name: 'Shampoo',
      price: 'Rp 10.000',
      sold_qty: 3030235,
      img_url: 'https://www.livingproof.com/dw/image/v2/BCMH_PRD/on/demandware.static/-/Sites-livingproof-catalog/default/dwd260af54/images/hi-res/pdp/Restore/Restore_shampoo_front.jpg?sw=650&sh=650&sm=fit&cx=200&cy=400&cw=1600&ch=1600',
    },
    {
      name: 'Sanitizer',
      price: 'Rp 10.000',
      sold_qty: 1030234,
      img_url: 'https://cdn.lovetiki.com/wp-content/uploads/2020/06/789_product.jpg',
    },
    {
      name: 'Masker',
      price: 'Rp 30.000',
      sold_qty: 530234,
      img_url: 'https://www.static-src.com/wcsstore/Indraprastha/images/catalog/full/evo-plusmed-_evo-plusmed--surgical-face-mask-4d-masker-medis---white--25-pcs-_full03.jpg',
    }
  ],
  topCompetitor: [
    {
      name: 'Masker',
      price: 'Rp 30.000',
      sold_qty: 30234,
      img_url: 'https://www.static-src.com/wcsstore/Indraprastha/images/catalog/full/evo-plusmed-_evo-plusmed--surgical-face-mask-4d-masker-medis---white--25-pcs-_full03.jpg',
    },
    {
      name: 'Sanitizer',
      price: 'Rp 10.000',
      sold_qty: 20234,
      img_url: 'https://cdn.lovetiki.com/wp-content/uploads/2020/06/789_product.jpg',
    },
    {
      name: 'Mineral Water',
      price: 'Rp 5.000',
      sold_qty: 10234,
      img_url: 'https://ecs7.tokopedia.net/img/cache/700/product-1/2019/6/11/1874896/1874896_754d1c3c-4c7a-4dd0-802c-6704abde7c94_554_554.jpg',
    },
    {
      name: 'Soap',
      price: 'Rp 23.000',
      sold_qty: 10231,
      img_url: 'https://upload.wikimedia.org/wikipedia/commons/9/9b/Handmade_soap_cropped_and_simplified.jpg',
    },
    {
      name: 'Shampoo',
      price: 'Rp 10.000',
      sold_qty: 516,
      img_url: 'https://www.livingproof.com/dw/image/v2/BCMH_PRD/on/demandware.static/-/Sites-livingproof-catalog/default/dwd260af54/images/hi-res/pdp/Restore/Restore_shampoo_front.jpg?sw=650&sh=650&sm=fit&cx=200&cy=400&cw=1600&ch=1600',
    },
  ]
}

export default products